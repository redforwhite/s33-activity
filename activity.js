fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {

  	let titles = json.map(json => json	.title)
  	console.log(titles)
 })

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(`The item ${json.title} on the list has a status of ${json.completed ? 'Completed' : 'Not Completed'}`))

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Walking for secretary',
		body: 'Flying to the moon',
		userId: 69
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: 'Pending',
		description: 'gg',
		status: 'Pending',
		title: 'Update to do list',
		userId: 69
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: '12/12/2069',
		status: 'Completed',
		title: 'Kumpleto na daw',
		userId: 69
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(response => response.json())
.then(json => console.log(json))